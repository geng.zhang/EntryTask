function login() {
    var username = document.getElementById("username")
    var password = document.getElementById("password")

    if (username.value == "") {
        alert("请输入用户名")
    } else if (password.value == "") {
        alert("请输入密码")
    }
    if (!username.value.match(/^\S{1,64}$/)) {
        console.log("get focus")
        username.className = 'userRed';
        username.focus();
        return;
    }

    if (password.value.length < 1 || password.value.length > 64) {
        console.log("passwd get focus")
        password.className = 'userRed';
        password.focus();
        return;
    }

    var data = {"user_name":username.value,"password":password.value}
    console.log(data)

    var xhr = new XMLHttpRequest();
    xhr.open('post', 'http://localhost:8080/login')
    // xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
    // xhr.send('username=' + username.value + "&passwd=" + password.value)
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.send(JSON.stringify(data))
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log(xhr.responseText)
                var json = eval("(" + xhr.responseText + ")");
                if(json.basic.code != 0) {
                    alert(json.basic.msg)
                    window.location.href = "http://localhost:8080/static/login.html"
                }
                else{
                    console.log(json.userid)
                    window.location.href = "http://localhost:8080/static/index.html?userid=" + json.token + "&username=" + username.value
                    window.event.returnValue = false
                }

            } else {
                alert("账号或密码错误。")
            }
        }
    }
}
