package main

import (
	"awesomeProject/proto/src"
	"database/sql"
	"fmt"
	"github.com/go-redis/redis"
	"google.golang.org/grpc"
	"log"
	"time"
)

var rdb *redis.Client
var conn *sql.DB
var grpccli *src.EntryTaskClient

// 初始化连接
func initRedisClient() (err error) {
	rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	_, err = rdb.Ping().Result()
	if err != nil {
		return err
	}
	return nil
}

func InitDBClient() error {
	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/user?charset=%s&parseTime=true&loc=Local",
		"root", "", "127.0.0.1:3306", "utf8")
	//fmt.Println(dsn)
	conn, err = sql.Open("mysql", dsn)
	if err != nil {
		return err
	} else {
		conn.SetConnMaxLifetime(7 * time.Second) //设置空闲时间，这个是比mysql 主动断开的时候短
		conn.SetMaxOpenConns(10)
		conn.SetMaxIdleConns(10)
		return nil
	}
}

func CallInit() error {

	conn, err := grpc.Dial("localhost:8081", grpc.WithInsecure())
	if err != nil {
		log.Fatal("did not connect: %v", err)
	}
	defer conn.Close()
	*grpccli = src.NewEntryTaskClient(conn)
	return nil
}
