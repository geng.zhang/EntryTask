package ApiSrv

import (
	"awesomeProject/client"
	"awesomeProject/proto/src"
	"github.com/golang/protobuf/proto"
	"google.golang.org/protobuf/encoding/protojson"
	"io/ioutil"
	"log"
	"net/http"
)

func UploadPicture(w http.ResponseWriter, r *http.Request) {

	userName := r.FormValue("user_name")
	token := r.Header.Get("Authorization")
	userPhoto, header, _ := r.FormFile("new_file")
	byt, _ := ioutil.ReadAll(userPhoto)

	//获取请求信息
	req := &src.UpdatePictureRequest{
		UserName:       proto.String(userName),
		Token:          proto.String(token),
		NewPictureName: proto.String(header.Filename),
		NewPicture:     byt,
	}

	//请求rpc层
	log.Println("UploadPicture req：", req)
	resp := client.CallUpdatePicture(*req)
	log.Println("UploadPicture resp：", &resp)

	msg, _ := protojson.Marshal(&resp)
	w.Write(msg)

}
