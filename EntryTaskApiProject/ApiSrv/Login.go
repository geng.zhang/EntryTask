package ApiSrv

import (
	"awesomeProject/client"
	"awesomeProject/proto/src"
	"awesomeProject/tools"
	"github.com/golang/protobuf/proto"
	"google.golang.org/protobuf/encoding/protojson"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

//用户登陆
func Login(w http.ResponseWriter, r *http.Request) {

	//获取请求信息
	var params = tools.GetRequestData(r)
	req := &src.LoginRequest{
		UserName: proto.String(params["user_name"]),
		Password: proto.String(params["password"]),
	}

	//压力测试test
	if params["session_id"] == "112233" {
		rand.Seed(time.Now().UnixNano())
		randomNum := rand.Intn(10000000)
		req.UserName = proto.String(params["user_name"] + strconv.Itoa(randomNum))
	}

	//请求rpc层
	log.Println("Login req：", req)
	resp := client.CallLogin(*req)
	log.Println("Login resp：", &resp)

	msg, _ := protojson.Marshal(&resp)
	w.Write(msg)

}
