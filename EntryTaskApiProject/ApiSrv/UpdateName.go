package ApiSrv

import (
	"awesomeProject/client"
	"awesomeProject/proto/src"
	"awesomeProject/tools"
	"github.com/golang/protobuf/proto"
	"google.golang.org/protobuf/encoding/protojson"
	"log"
	"net/http"
)

func UpdateNickname(w http.ResponseWriter, r *http.Request) {

	//获取请求信息
	var params = tools.GetRequestData(r)
	req := &src.UpdateNickNameRequest{
		UserName:    proto.String(params["user_name"]),
		NewNickName: proto.String(params["new_nick_name"]),
		Token:       proto.String(r.Header.Get("Authorization")),
	}

	//请求rpc层
	log.Println("UpdateNickname req：", req)
	resp := client.CallUpdateNickName(*req)
	log.Println("UpdateNickname resp：", &resp)

	msg, _ := protojson.Marshal(&resp)
	w.Write(msg)

}
