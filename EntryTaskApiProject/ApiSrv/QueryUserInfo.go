package ApiSrv

import (
	"awesomeProject/client"
	"awesomeProject/proto/src"
	"awesomeProject/tools"
	"github.com/golang/protobuf/proto"
	"google.golang.org/protobuf/encoding/protojson"
	"log"
	"net/http"
)

func QueryUserinfo(w http.ResponseWriter, r *http.Request) {

	//获取请求信息
	var params = tools.GetRequestData(r)
	req := &src.QueryUserInfoRequest{
		UserName: proto.String(params["user_name"]),
		Token:    proto.String(r.Header.Get("Authorization")),
	}

	//请求rpc层
	log.Println("QueryUserinfo req：", req)
	resp := client.CallQueryUserInfo(*req)
	log.Println("QueryUserinfo resp：", &resp)

	msg, _ := protojson.Marshal(&resp)
	w.Write(msg)

}
