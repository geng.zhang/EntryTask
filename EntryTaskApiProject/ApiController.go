package main

import (
	"awesomeProject/ApiSrv"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

var extensionToContentType = map[string]string{
	".html": "text/html; charset=utf-8",
	".css":  "text/css; charset=utf-8",
	".js":   "application/javascript",
	".xml":  "text/xml; charset=utf-8",
	".jpg":  "image/jpeg",
}

func getLoginHtml(w http.ResponseWriter, r *http.Request) {

	path := "." + r.URL.Path
	fmt.Println(path)

	f, err := os.Open(path)
	if err != nil {
		return
	}
	defer f.Close()

	d, err := f.Stat()
	if err != nil {
		return
	}

	if d.IsDir() {
		return
	}

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return
	}

	ext := filepath.Ext(path)
	if contentType := extensionToContentType[ext]; contentType != "" {
		w.Header().Set("Content-Type", contentType)
	}

	w.Header().Set("Content-Length", strconv.FormatInt(d.Size(), 10))
	w.Write(data)

}

func main() {

	http.HandleFunc("/static/", getLoginHtml)

	http.HandleFunc("/login", ApiSrv.Login)
	http.HandleFunc("/query_userinfo", ApiSrv.QueryUserinfo)
	http.HandleFunc("/upload_picture", ApiSrv.UploadPicture)
	http.HandleFunc("/update_nickname", ApiSrv.UpdateNickname)
	// 监听本机的8080端口
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("Listen Error: ", err)
	}
}
