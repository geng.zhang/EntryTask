package tools

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func GetRequestData(req *http.Request) map[string]string {
	// 根据请求body创建一个json解析器实例
	decoder := json.NewDecoder(req.Body)
	// 用于存放参数key=value数据
	var params map[string]string
	// 解析参数 存入map
	decoder.Decode(&params)
	fmt.Println("请求体:", params)
	return params
}
