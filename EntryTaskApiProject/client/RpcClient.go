package client

import (
	"awesomeProject/proto/src"
	"context"
	"github.com/golang/protobuf/proto"
	"log"
)

//func Call(request []byte, callName string) []byte {
//
//	client, err := rpc.Dial("tcp", "localhost:8081")
//	if err != nil {
//		log.Fatal("dialing:", err)
//	}
//	methodstr := "EntryTaskService." + callName
//	var reply []byte
//	err = client.Call(methodstr, request, &reply)
//	if err != nil {
//		log.Fatal(err)
//	}
//	fmt.Println(reply)
//	return reply
//}

func CallLogin(request src.LoginRequest) src.LoginResponse {

	clientPool := GetScp()
	response := &src.LoginResponse{
		Basic: nil,
		Token: nil,
	}
	err := clientPool.Invoke(context.Background(), "/task.EntryTask/Login", nil, &request, response)
	if err != nil {
		log.Println("CallLogin error：", err)
		response.Basic.Code = proto.Int32(99)
		response.Basic.Msg = proto.String("系统异常,请重试")
		return *response
	}
	log.Printf("CallLogin response: %s", response)
	return *response
}

func CallQueryUserInfo(request src.QueryUserInfoRequest) src.QueryUserInfoResponse {

	clientPool := GetScp()
	response := &src.QueryUserInfoResponse{
		Basic: nil,
	}
	err := clientPool.Invoke(context.Background(), "/task.EntryTask/QueryUserInfo", nil, &request, response)
	if err != nil {
		log.Println("CallQueryUserInfo error：", err)
		response.Basic.Code = proto.Int32(99)
		response.Basic.Msg = proto.String("系统异常,请重试")
		return *response
	}
	log.Printf("CallLogin response: %s", response)
	return *response
}

func CallUpdateNickName(request src.UpdateNickNameRequest) src.UpdateNickNameResponse {

	clientPool := GetScp()
	response := &src.UpdateNickNameResponse{
		Basic: nil,
	}
	err := clientPool.Invoke(context.Background(), "/task.EntryTask/UpdateNickName", nil, &request, response)
	if err != nil {
		log.Println("Call CallUpdateNickName error：", err)
		response.Basic.Code = proto.Int32(99)
		response.Basic.Msg = proto.String("系统异常,请重试")
		return *response
	}
	log.Printf("Call CallUpdateNickName response: %s", response)
	return *response
}

func CallUpdatePicture(request src.UpdatePictureRequest) src.UpdatePictureResponse {

	clientPool := GetScp()
	response := &src.UpdatePictureResponse{
		Basic: nil,
	}
	err := clientPool.Invoke(context.Background(), "/task.EntryTask/UpdatePicture", nil, &request, response)
	if err != nil {
		log.Println("Call UpdatePicture error：", err)
		response.Basic.Code = proto.Int32(99)
		response.Basic.Msg = proto.String("系统异常,请重试")
		return *response
	}
	log.Printf("Call UpdatePicture response: %s", response)
	return *response
}
