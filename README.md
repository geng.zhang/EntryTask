<!-- 修订历史
## 一、背景及目的
## 二、逻辑架构设计
## 三、核心逻辑详细设计
## 四、接口设计
## 五、存储设计
## 六、外部依赖与限制
## 七、部署方案与环境要求
## 八、SLA
## 九、遗留问题与风险预估
## 十、附录 -->


 ### 修订历史
| 版本 | 修订日期 | 修订者  | 修订内容 |
|---- |:----:| ----:| ----:|
|  v1  | 2022.5.13 | 张庚 | 新建 |
			

 ### 一、背景及目的

* 让团队更好地了解新人对技能的掌握情况
* 熟悉简单的Web API后台架构
* 熟悉使用Go实现HTTP API（JSON、文件）
* 熟悉使用Go实现基于TCP的RPC框架（设计和实现通信协议）
* 熟悉基于Auth Token的鉴权机制和流程
* 熟悉使用Go对MySQL、Redis进行基本操作
* 对任务进度和时间有所意识
* 对代码规范、测试、文档、性能调优需要有所意识


 ### 二、逻辑架构设计

![1.png](./image/1.png)



 ### 三、核心逻辑详细设计

 #### 3.1 用户登陆
* 用户在前端输入账号和密码之后，后端进行账号和密码的校验，成功之后返回token（token是含有效期的）。其中用户密码存储在db当中，加盐之后进行md5可以保证密码安全

![2.png](./image/2.png)

 #### 3.2 查询用户信息
* 根据校验token和用户账户，成功之后返回用户信息（用户名，昵称，头像url）

![3.png](image/3.png)

 #### 3.3 修改用户头像
* 前端传入头像文件，后端校验token和用户信息，成功之后将用户头像更新为此次上传的头像

![4.png](image/4.png)

 #### 3.4 修改用户昵称
* 前端修改昵称传给后端，后端校验token和用户信息，成功之后将用户昵称更新，如果缓存中存在用户信息则删除缓存

![5.png](image/5.png)


 ### 四、接口设计

 #### 4.1 用户登陆
 
* URL：/login
* 请求方法：POST
* 请求参数:

| name | type | required  | desc |
|---- |:----:| ----:| ----:|
|  user_name  | string | Y | 账号 |
|  password  | string | Y | 密码 |

* 响应参数：

| name | type | desc |
|---- |:----:| ----:|
|  basic  | Basic | 公共参数 |
|  token  | string | token |

* Basic:

| name | type | desc |
|---- |:----:| ----:|
|  code  | int | 响应码 |
|  msg  | string | 响应描述 |



#### 4.2 查询用户信息
* URL：/query_userinfo
* 请求方法：POST
* 请求参数：

| name | type | required  | desc |
|---- |:----:| ----:| ----:|
|  user_name  | string | Y | 账号 |
|  token  | string | Y | token |

* 响应参数：

| name | type | desc |
|---- |:----:| ----:|
|  basic  | Basic | 公共参数 |
|  user_name  | string | 用户名 |
|  nick_name  | string | 昵称 |
|  picture_url  | string | 头像url |

* Basic:

| name | type | desc |
|---- |:----:| ----:|
|  code  | int | 响应码 |
|  msg  | string | 响应描述 |



#### 4.3 修改昵称
* URL：/update_nickname
* 请求方法：POST
* 请求参数：

| name | type | required  | desc |
|---- |:----:| ----:| ----:|
|  user_name  | string | Y | 账号 |
|  token  | string | Y | token |
|  new_nick_name  | string | Y | 新昵称 |

* 响应参数：

| name | type | desc |
|---- |:----:| ----:|
|  basic  | Basic | 公共参数 |

* Basic:

| name | type | desc |
|---- |:----:| ----:|
|  code  | int | 响应码 |
|  msg  | string | 响应描述 |
 
 

#### 4.4 修改头像
* URL：/update_picture
* 请求方法：POST
* 请求参数：

| name | type | required  | desc |
|---- |:----:| ----:| ----:|
|  user_name  | string | Y | 账号 |
|  token  | string | Y | token |
|  new_picture_name  | string | Y | 头像文件名 |
|  new_picture  | bytes | Y | 头像文件 |

* 响应参数：

| name | type | desc |
|---- |:----:| ----:|
|  basic  | Basic | 公共参数 |

* Basic:

| name | type | desc |
|---- |:----:| ----:|
|  code  | int | 响应码 |
|  msg  | string | 响应描述 |

 ### 五、存储设计


DB库表定义:
```sql
CREATE TABLE user.t_user_info (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(40) NOT NULL,
  `nick_name` varchar(40) NOT NULL,
  `picture` varchar(1024) NOT NULL,
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
```




 ### 六、外部依赖与限制






 ### 七、部署方案与环境要求




 ### 八、SLA





 ### 九、遗留问题与风险预估





 ### 十、附录

压测报告地址：
https://docs.google.com/document/d/1jmmGDIOPaY_FlqRf1eTk9kJXZgnM0cVgVYM7PUAfRBs/edit#











