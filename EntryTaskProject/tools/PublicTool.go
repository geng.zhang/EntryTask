package tools

import (
	"EntryTaskProject/db"
	"EntryTaskProject/proto/src"
	"EntryTaskProject/pubinit"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"google.golang.org/protobuf/proto"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var expire = 300

func Md5Proc(password string) string {
	h := md5.New()
	h.Write([]byte("123"))
	h.Write([]byte(password))
	//io.WriteString(h, )
	md5str := fmt.Sprintf("%x", h.Sum(nil))
	fmt.Printf("%s", md5str)
	return md5str
}

func HasSuffix(s, suffix string) bool {
	return len(s) >= len(suffix) && s[len(s)-len(suffix):] == suffix
}

func CreateToken(userName string) string {
	srcstr := strconv.FormatInt(time.Now().Unix(), 10) + userName
	h := md5.New()
	h.Write([]byte(srcstr))
	//io.WriteString(h, )
	md5str := fmt.Sprintf("%x", h.Sum(nil))

	_, err := pubinit.Rdb.EvalSha(pubinit.LuaHash, []string{userName + "token", userName + "token", md5str}).Result()
	if err != nil {
		return ""
	}
	//luaScript.Run(pubinit.Rdb, []string{userName + "token", string(expire)})
	//pubinit.Rdb.SetNX(userName+"token", md5str, time.Minute*5)
	return md5str
}

func AuthCheck(token string, userName string) src.Basic {

	var basic src.Basic

	if len(token) <= 0 {
		basic.Code = proto.Int32(999)
		basic.Msg = proto.String("缺少鉴权信息")
		return basic
	}

	val, err := pubinit.Rdb.Get(userName + "token").Result()
	if err != nil || len(val) <= 0 || val != token {
		basic.Code = proto.Int32(998)
		basic.Msg = proto.String("鉴权失败,请重新登陆")
		return basic
	}
	basic.Code = proto.Int32(0)
	return basic
}

func getRequestFile(req *http.Request) []byte {
	req.ParseForm()                            //解析表单
	imgFile, _, err := req.FormFile("picture") //获取文件内容
	if err != nil {
		log.Fatal(err)
	}
	defer imgFile.Close()

	imgName := ""
	files := req.MultipartForm.File //获取表单中的信息
	for k, v := range files {
		for _, vv := range v {
			fmt.Println(k + ":" + vv.Filename) //获取文件名
			if strings.Index(vv.Filename, ".png") > 0 {
				imgName = vv.Filename
			}
		}
	}

	saveFile, _ := os.Create(imgName)
	defer saveFile.Close()
	io.Copy(saveFile, imgFile) //保存
	return nil
	//w.Write([]byte("successfully saved"))
}

func getRequestData(req *http.Request) map[string]string {
	// 根据请求body创建一个json解析器实例
	decoder := json.NewDecoder(req.Body)
	// 用于存放参数key=value数据
	var params map[string]string
	// 解析参数 存入map
	decoder.Decode(&params)
	fmt.Println("请求体:", params)
	return params
}

func resppnseInfo2Json(resp db.ResppnseInfo, code int, msg string) []byte {
	if code >= 0 && len(msg) > 0 {
		resp.Code = code
		resp.Msg = msg
	}
	jsonMsg, err := json.Marshal(&resp)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Println(string(jsonMsg))
	return jsonMsg
}
