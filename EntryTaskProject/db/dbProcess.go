package db

import (
	"EntryTaskProject/pubinit"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

type User struct {
	Id       int64
	UserName string
	NickName string
	Picture  string
	Password string
}

type ResppnseInfo struct {
	Code     int
	Msg      string
	Token    string
	NickName string
	Picture  string
}

func QueryByUserName(userName string) *User {
	tmps := "select * from user.t_user_info where user_name = ?"
	row := pubinit.Conn.QueryRow(tmps, userName)
	var u User
	err := row.Scan(&u.Id, &u.UserName, &u.NickName, &u.Picture, &u.Password)
	if err != nil {
		log.Println(err.Error())
		return nil
	}
	fmt.Println(u)
	return &u
}

func UpdateNickName(userName string, newNickName string) bool {
	updates := "update user.t_user_info set nick_name = \"" + newNickName + "\" where user_name = \"" + userName + "\""
	result, err := pubinit.Conn.Exec(updates)
	if err != nil {
		log.Println("update newNickName error")
		return false
	}
	count, err := result.RowsAffected()
	if count != 1 {
		log.Println("update newNickName count error")
		return false
	}
	return true
}

//func UpdateOne(userName string, nickName string, photo []byte) string {
//
//	tmps := fmt.Sprintf("select * from user.t_user_info where user_name = \"%s\"", userName)
//	fmt.Println(tmps)
//	row := pubinit.Conn.QueryRow(tmps)
//	var u User
//	err := row.Scan(&u.Id, &u.UserName, &u.NickName, &u.Picture, &u.Password)
//	if err != nil {
//		fmt.Println(err.Error())
//		return err.Error()
//	}
//	updates := "update user.t_user_info "
//	if nickName != "" {
//		updates = "set nick_name = \"" + nickName + "\" "
//	}
//
//	result, err := pubinit.Conn.Exec(updates)
//	if err != nil {
//		fmt.Println("update error")
//		return err.Error()
//	}
//	count, err := result.RowsAffected()
//	if count != 1 {
//		counterr := "update count error, count:" + strconv.FormatInt(count, 10)
//		fmt.Println("update count error")
//		return counterr
//	}
//	fmt.Println(count)
//	return ""
//}
