package db

import (
	"EntryTaskProject/pubinit"
	"encoding/json"
	"time"
)

func QueryUserInfoByRedis(userName string) *User {

	val, err := pubinit.Rdb.Get(userName + "info").Result()
	if err != nil || len(val) <= 0 {
		return nil
	}

	var user User
	err1 := json.Unmarshal([]byte(val), &user)
	if err1 != nil {
		return nil
	}
	return &user
}

func SetUserInfoByRedis(user User) {
	user.Password = ""
	jsonMsg, _ := json.Marshal(user)

	_, err := pubinit.Rdb.SetNX(user.UserName+"info", string(jsonMsg), time.Minute*30).Result()
	if err != nil {
		return
	}
}

func DelUserInfoByRedis(userName string) {

	_, _ = pubinit.Rdb.Del(userName + "info").Result()
}
