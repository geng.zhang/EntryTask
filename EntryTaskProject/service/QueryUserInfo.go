package service

import (
	"EntryTaskProject/db"
	"EntryTaskProject/proto/src"
	"EntryTaskProject/tools"
	"fmt"
	"google.golang.org/protobuf/proto"
)

func QueryUserinfoCheck(request src.QueryUserInfoRequest) src.QueryUserInfoResponse {

	if len(request.GetUserName()) == 0 {
		return setQueryUserInfoResp(201, "缺少用户名信息")
	}

	if len(request.GetUserName()) > 20 || len(request.GetUserName()) < 4 {
		return setQueryUserInfoResp(202, "用户名长度必须为6～20个字符")
	}

	var resp src.QueryUserInfoResponse
	var basic = tools.AuthCheck(request.GetToken(), request.GetUserName())
	resp.Basic = &basic
	return resp
}

func QueryUserinfo(request src.QueryUserInfoRequest) src.QueryUserInfoResponse {

	//校验
	resp := QueryUserinfoCheck(request)
	fmt.Printf("查询用户信息校验:%v\n", resp)
	if resp.GetBasic().GetCode() != 0 {
		fmt.Println(resp)
		return resp
	}

	//
	user := db.QueryUserInfoByRedis(request.GetUserName())
	if user != nil {
		return setSuccResp(*user)
	}
	//查询db
	user = db.QueryByUserName(request.GetUserName())
	if user == nil {
		return setQueryUserInfoResp(99, "未找到该用户")
	}
	db.SetUserInfoByRedis(*user)
	return setSuccResp(*user)
}

func setSuccResp(user db.User) src.QueryUserInfoResponse {
	resp := setQueryUserInfoResp(0, "成功")
	resp.UserName = proto.String(user.UserName)
	resp.NickName = proto.String(user.NickName)
	resp.PictureUrl = proto.String(user.Picture)
	return resp
}

func setQueryUserInfoResp(code int32, msg string) src.QueryUserInfoResponse {
	resp := &src.QueryUserInfoResponse{}
	if code >= 0 && len(msg) > 0 {
		basic := &src.Basic{}
		basic.Code = proto.Int32(code)
		basic.Msg = proto.String(msg)
		resp.Basic = basic
	}
	return *resp
}
