package service

import (
	"EntryTaskProject/db"
	"EntryTaskProject/proto/src"
	"EntryTaskProject/tools"
	"fmt"
	"google.golang.org/protobuf/proto"
)

func LoginProc(request src.LoginRequest) src.LoginResponse {

	//请求校验
	resp := LoginCheck(request)
	if *resp.Basic.Code != 0 {
		fmt.Println(resp)
		return resp
	}

	//查询db
	user := db.QueryByUserName(request.GetUserName())
	if user == nil {
		return SetLoginResp(99, "未找到该用户")
	}

	//密码md5处理并进行比对
	md5str := tools.Md5Proc(request.GetPassword())
	if md5str != user.Password {
		return SetLoginResp(98, "密码错误")
	}

	//密码校验成功返回token
	resp = SetLoginResp(0, "success")
	token := tools.CreateToken(user.UserName)
	if token == "" {
		return SetLoginResp(997, "获取token失败")
	}
	resp.Token = proto.String(token)
	return resp
}

func LoginCheck(request src.LoginRequest) src.LoginResponse {

	if len(request.GetUserName()) == 0 || len(request.GetPassword()) == 0 {
		return SetLoginResp(100, "请输入用户名和密码")
	}

	if len(request.GetUserName()) > 20 || len(request.GetUserName()) < 4 {
		return SetLoginResp(101, "用户名长度必须为6～20个字符")
	}

	if len(request.GetPassword()) > 20 || len(request.GetPassword()) < 6 {
		return SetLoginResp(102, "密码长度必须为6～20个字符")
	}
	return SetLoginResp(0, "check success")

}

func SetLoginResp(code int32, msg string) src.LoginResponse {
	resp := &src.LoginResponse{}
	if code >= 0 && len(msg) > 0 {
		basic := &src.Basic{}
		basic.Code = proto.Int32(code)
		basic.Msg = proto.String(msg)
		resp.Basic = basic
	}
	return *resp
}
