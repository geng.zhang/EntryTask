package service

import (
	"EntryTaskProject/db"
	"EntryTaskProject/proto/src"
	"EntryTaskProject/tools"
	"bytes"
	"fmt"
	"google.golang.org/protobuf/proto"
	"io"
	"os"
)

func UpdatePictureProc(request src.UpdatePictureRequest) src.UpdatePictureResponse {

	//请求校验
	resp := updatePictureCheck(request)
	if resp.GetBasic().GetCode() != 0 {
		fmt.Println(resp)
		return resp
	}
	//查询db
	user := db.QueryByUserName(request.GetUserName())
	if user == nil {
		return setUpdatePictureResp(99, "未找到该用户")
	}
	//更新头像文件
	return UpdatePictureFile(request)
}

func updatePictureCheck(request src.UpdatePictureRequest) src.UpdatePictureResponse {

	if len(request.GetUserName()) == 0 {
		return setUpdatePictureResp(401, "更改头像未传入账号信息")
	}
	if request.GetNewPicture() == nil {
		return setUpdatePictureResp(402, "未传入新头像")
	}

	if !tools.HasSuffix(request.GetNewPictureName(), ".jpg") && !tools.HasSuffix(request.GetNewPictureName(), ".png") {
		return setUpdatePictureResp(403, "不支持的头像格式")
	}

	var resp src.UpdatePictureResponse
	var basic = tools.AuthCheck(request.GetToken(), request.GetUserName())
	resp.Basic = &basic
	return resp
}

func UpdatePictureFile(request src.UpdatePictureRequest) src.UpdatePictureResponse {
	//创建一个空文件
	fileName := request.GetUserName() + request.GetNewPictureName()[len(request.GetNewPictureName())-4:]
	dst, er := os.Create("/Users/geng.zhang/GolandProjects/awesomeProject/static/images/" + fileName)
	if er != nil {
		return setUpdatePictureResp(404, "更新头像失败,请重试")
	}
	defer dst.Close()
	//将获取到的文件复制给创建的文件,如果用户存在头像则会覆盖
	_, err := io.Copy(dst, bytes.NewReader(request.GetNewPicture()))
	if err != nil {
		return setUpdatePictureResp(404, "更新头像失败,请重试")
	}
	return setUpdatePictureResp(0, "修改成功")
}

func setUpdatePictureResp(code int32, msg string) src.UpdatePictureResponse {
	resp := &src.UpdatePictureResponse{}
	if code >= 0 && len(msg) > 0 {
		basic := &src.Basic{}
		basic.Code = proto.Int32(code)
		basic.Msg = proto.String(msg)
		resp.Basic = basic
	}
	return *resp
}
