package service

import (
	"EntryTaskProject/db"
	"EntryTaskProject/proto/src"
	"EntryTaskProject/tools"
	"fmt"
	"google.golang.org/protobuf/proto"
)

func UpdateNameCheck(request src.UpdateNickNameRequest) src.UpdateNickNameResponse {

	if len(request.GetNewNickName()) > 20 || len(request.GetNewNickName()) < 4 {
		return SetUpdateNicknameResp(301, "新昵称长度必须为4～20个字符")
	}

	var resp src.UpdateNickNameResponse
	var basic = tools.AuthCheck(request.GetToken(), request.GetUserName())
	resp.Basic = &basic
	return resp
}

func UpdateNickname(request src.UpdateNickNameRequest) src.UpdateNickNameResponse {

	//请求校验
	resp := UpdateNameCheck(request)
	if resp.GetBasic().GetCode() != 0 {
		fmt.Println(resp)
		return resp
	}
	//查询db
	user := db.QueryByUserName(request.GetUserName())
	if user == nil {
		return SetUpdateNicknameResp(99, "未找到该用户")
	}
	//db修改
	if request.GetNewNickName() != user.NickName {
		isSucc := db.UpdateNickName(user.UserName, request.GetNewNickName())
		if isSucc == false {
			return SetUpdateNicknameResp(302, "更新失败,请稍后重试")
		}
	}
	//del缓存数据
	db.DelUserInfoByRedis(request.GetUserName())
	return SetUpdateNicknameResp(0, "修改成功")
}

func SetUpdateNicknameResp(code int32, msg string) src.UpdateNickNameResponse {
	resp := &src.UpdateNickNameResponse{}
	if code >= 0 && len(msg) > 0 {
		basic := &src.Basic{}
		basic.Code = proto.Int32(code)
		basic.Msg = proto.String(msg)
		resp.Basic = basic
	}
	return *resp
}
