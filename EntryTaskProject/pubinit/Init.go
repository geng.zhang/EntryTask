package pubinit

import (
	"database/sql"
	"fmt"
	"github.com/go-redis/redis"
	"time"
)

var Rdb *redis.Client
var Conn *sql.DB

var LuaHash string

var script = `
		redis.call("del", KEYS[1])
		local res = redis.call("set", KEYS[2], KEYS[3])
	    return res
	`

// 初始化连接
func InitRedisClient() (err error) {
	Rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	_, err = Rdb.Ping().Result()
	if err != nil {
		return err
	}
	LuaHash, _ = Rdb.ScriptLoad(script).Result() //返回的脚本会产生一个sha1哈希值,下次用的时候可以直接使用这个值
	return nil
}

func InitDBClient() error {
	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/user?charset=%s&parseTime=true&loc=Local",
		"root", "", "127.0.0.1:3306", "utf8mb4")
	//fmt.Println(dsn)
	Conn, err = sql.Open("mysql", dsn)
	if err != nil {
		return err
	} else {
		Conn.SetConnMaxLifetime(7 * time.Second) //设置空闲时间，这个是比mysql 主动断开的时候短
		Conn.SetMaxOpenConns(10)
		Conn.SetMaxIdleConns(10)
		
		return nil
	}
}
