package main

import (
	"EntryTaskProject/proto/src"
	"EntryTaskProject/pubinit"
	"EntryTaskProject/service"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"net"
)

type EntryTaskServer struct{}

func (s *EntryTaskServer) MustEmbedUnimplementedEntryTaskServer() {}

func (s *EntryTaskServer) Login(ctx context.Context, request *src.LoginRequest) (*src.LoginResponse, error) {
	resp := service.LoginProc(*request)
	return &resp, nil
}

func (s *EntryTaskServer) QueryUserInfo(ctx context.Context, request *src.QueryUserInfoRequest) (*src.QueryUserInfoResponse, error) {
	resp := service.QueryUserinfo(*request)
	return &resp, nil
}

func (s *EntryTaskServer) UpdateNickName(ctx context.Context, request *src.UpdateNickNameRequest) (*src.UpdateNickNameResponse, error) {
	resp := service.UpdateNickname(*request)
	return &resp, nil
}

func (s *EntryTaskServer) UpdatePicture(ctx context.Context, request *src.UpdatePictureRequest) (*src.UpdatePictureResponse, error) {
	resp := service.UpdatePictureProc(*request)
	return &resp, nil
}

func main() {

	if pubinit.Conn == nil {
		err := pubinit.InitDBClient()
		if err != nil {
			log.Fatal("InitDBClient error;", err)
			return
		}
	}
	if pubinit.Rdb == nil {
		err := pubinit.InitRedisClient()
		if err != nil {
			log.Fatal("InitRedisClient error;", err)
			return
		}
	}

	listener, err := net.Listen("tcp", ":8081")
	if err != nil {
		log.Fatal("ListenTCP error:", err)
		return
	}
	s := grpc.NewServer()
	src.RegisterEntryTaskServer(s, &EntryTaskServer{})
	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
